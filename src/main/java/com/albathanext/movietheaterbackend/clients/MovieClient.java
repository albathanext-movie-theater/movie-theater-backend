package com.albathanext.movietheaterbackend.clients;

import com.albathanext.movietheaterbackend.pojos.Movie;
import com.albathanext.movietheaterbackend.pojos.MoviesResponse;
import com.albathanext.movietheaterbackend.pojos.ReviewsResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class MovieClient  {

    @Value("${API_KEY:API_KEY_NOT_FOUND}")
    private String API_KEY;

    private final WebClient client;

    public MovieClient(WebClient.Builder builder) {
        this.client = builder.baseUrl("https://api.themoviedb.org/3/").build();
    }

    public Mono<MoviesResponse> getMovies() {
        return this.client.get().uri(uriBuilder -> uriBuilder
                .path("discover/movie")
                .queryParam("api_key", API_KEY)
                .queryParam("sort_by", "popularity.desc")
                .build()).accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(MoviesResponse.class);

    }

    public Mono<MoviesResponse> search(String query) {
        return this.client.get().uri(uriBuilder -> uriBuilder
                        .path("search/movie")
                        .queryParam("api_key", API_KEY)
                        .queryParam("query", query)
                        .build()).accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(MoviesResponse.class);

    }

    public Mono<Movie> getDetails(String movieId) {
        return this.client.get().uri(uriBuilder -> uriBuilder
                        .path("/movie/{movieId}")
                        .queryParam("api_key", API_KEY)
                        .build(movieId)).accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Movie.class);

    }

    ///movie/{movie_id}/reviews
    public Mono<ReviewsResponse> getReviews(String movieId) {
        return this.client.get().uri(uriBuilder -> uriBuilder
                        .path("/movie/{movieId}/reviews")
                        .queryParam("api_key", API_KEY)
                        .build(movieId)).accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(ReviewsResponse.class);

    }

}
