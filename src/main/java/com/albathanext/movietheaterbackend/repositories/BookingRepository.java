package com.albathanext.movietheaterbackend.repositories;

import com.albathanext.movietheaterbackend.models.Booking;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface BookingRepository extends MongoRepository<Booking, String> {
    List<Booking> findByEmail(String paramString);

    List<Booking> findByEmailAndMovieId(String paramString, long movieId);

    List<Booking> findByFirstNameLikeOrLastNameLikeOrEmailLike(String paramString, String paramString2, String paramString3);

}

