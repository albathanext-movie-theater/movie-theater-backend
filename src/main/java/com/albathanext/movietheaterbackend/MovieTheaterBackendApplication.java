package com.albathanext.movietheaterbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class MovieTheaterBackendApplication {



	public static void main(String[] args) {
		SpringApplication.run(MovieTheaterBackendApplication.class, args);
	}

}
