package com.albathanext.movietheaterbackend.pojos;

import java.util.List;

public class ReviewsResponse {

    private int page;
    private List<Review> results;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<Review> getResults() {
        return results;
    }

    public void setResults(List<Review> results) {
        this.results = results;
    }
}
