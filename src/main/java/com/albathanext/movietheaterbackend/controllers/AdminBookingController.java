package com.albathanext.movietheaterbackend.controllers;

import com.albathanext.movietheaterbackend.models.Booking;
import com.albathanext.movietheaterbackend.repositories.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"/api/v1/admin/bookings"})
public class AdminBookingController {

    @Autowired
    private BookingRepository bookingRepository;

    @GetMapping()
    public List<Booking> getAll() {

        return bookingRepository.findAll(Sort.by(Sort.Order.desc("bookedAt")));
    }

    @GetMapping({"/search"})
    public List<Booking> search(@RequestParam("query") String query) {
        return bookingRepository.findByFirstNameLikeOrLastNameLikeOrEmailLike(query, query, query);
    }

    @PatchMapping({"/cancel/{bookingId}"})
    public void update(@PathVariable String bookingId) {

        var bookingOptional = bookingRepository.findById(bookingId);
        if (bookingOptional.isEmpty()) {
            return;
        }

        var booking = bookingOptional.get();
        booking.setActive(false);
        bookingRepository.save(booking);
    }
}
