package com.albathanext.movietheaterbackend.controllers;

import com.albathanext.movietheaterbackend.clients.MovieClient;
import com.albathanext.movietheaterbackend.pojos.Movie;
import com.albathanext.movietheaterbackend.pojos.Review;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping({"/api/v1/movies"})
public class MovieController {

    @Autowired
    MovieClient movieClient;

    @GetMapping("")
    public List<Movie> movies() {
     
        return movieClient.getMovies().block().getResults().stream().limit(10).collect(Collectors.toList());

    }

    @GetMapping("/search")
    public List<Movie> searchMovie(@RequestParam("query") String query) {
        return movieClient.search(query).block().getResults().stream().limit(10).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public Movie getDetails(@PathVariable String id) {
        return movieClient.getDetails(id).block();

    }

    @GetMapping("/{id}/reviews")
    public List<Review> getReviews(@PathVariable String id) {
        return movieClient.getReviews(id).block().getResults();

    }
}
