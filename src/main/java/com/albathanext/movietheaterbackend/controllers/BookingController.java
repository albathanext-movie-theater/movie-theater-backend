package com.albathanext.movietheaterbackend.controllers;


import com.albathanext.movietheaterbackend.models.Booking;
import com.albathanext.movietheaterbackend.repositories.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({"/api/v1/bookings"})

public class BookingController {

    @Autowired
    private BookingRepository bookingRepository;

    @PostMapping()
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Booking> create(@RequestBody Booking booking) {

        var bookings = this.bookingRepository.findByEmailAndMovieId(booking.getEmail(), booking.getMovieId());


        var totalNumberOfBookedSeats = bookings.stream().filter(bookingIter -> bookingIter.isActive())
                .mapToInt(bookingIter -> bookingIter.getNumberOfSeats()).sum();
        var totalNumberOfSeats = totalNumberOfBookedSeats + booking.getNumberOfSeats();
        if (totalNumberOfSeats > 10) {
            return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build();
        }

        var bookingSaveResponse = this.bookingRepository.save(booking);
        return ResponseEntity.ok(bookingSaveResponse);
    }

    @GetMapping()
    public List<Booking> get(@RequestParam("email") String email) {
        return bookingRepository.findByEmail(email);
    }


    @PutMapping()
    public void update(@RequestBody Booking booking) {

        bookingRepository.save(booking);
    }

}
