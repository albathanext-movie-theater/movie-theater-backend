package com.albathanext.movietheaterbackend.models;

import java.util.UUID;

public class Booking {

    private String id =  UUID.randomUUID().toString();

    private String email;
    private String firstName;
    private String lastName;

    private long movieId;
    private long showTimeAt;
    private long bookedAt;

    private boolean isActive = true;
    private int numberOfSeats = 0;

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getMovieId() {
        return movieId;
    }

    public void setMovieId(long movieId) {
        this.movieId = movieId;
    }

    public long getShowTimeAt() {
        return showTimeAt;
    }

    public void setShowTimeAt(long showTimeAt) {
        this.showTimeAt = showTimeAt;
    }

    public long getBookedAt() {
        return bookedAt;
    }

    public void setBookedAt(long bookedAt) {
        this.bookedAt = bookedAt;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
