# Movie Theater Backend

The backend for the Movie Theater App

## Stack & Framework

The app is developed Springboot

## Environment variables

The `API_KEY` should be stored as an environment variable before you can run the app
